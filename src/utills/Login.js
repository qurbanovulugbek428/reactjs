import React, { Component } from 'react'
import {loginApi} from '../api/ApiHelper';
import {ACCESS_TOKEN, USER_ID} from '../constants/enams';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save'

export default class Login extends Component {


    constructor(props) {
        super(props)
    
        this.state = {
            isShown: 'password',
            email: '',
            password: ''
        }
    }
    
    change =(e)=>{
        this.setState({[e.target.name]:e.target.value});
    }

    onSubmit =(e)=>{
        e.preventDefault();
        const A={
            email: this.state.email,
            password: this.state.password
        }

        loginApi(A).then(res =>{
            console.log(res)
            if(res.status === 200){
                console.log(res.headers.authorization);
                localStorage.setItem(ACCESS_TOKEN, res.headers.authorization);
                localStorage.setItem(USER_ID, res.headers.userid);
                // window.location.reload();
                this.props.onLogin();
            }
            
        })
    }


    render() {
       
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    {/* <input type="email" name="email" value={this.state.email} onChange={this.change} placeholder="enter your email"></input> */}
                
                    <TextField 
                    // id="outlined-basic" 
                    type="email"
                    name="email"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={this.change}
                    label="Email" 
                    variant="outlined" 
                    size="small"
                    margin ='dense'
                    />


                    <TextField 
                    // id="outlined-basic" 
                    type={this.state.isShown}
                    name="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.change}
                    label="Password" 
                    variant="outlined"
                    size="small"
                    margin ='dense' 
                    />

                    {/* <input type={this.state.isShown} value={this.state.password}  onChange={this.change} name="password" placeholder="enter your password"></input> */}
                
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        margin ='dense'
                        startIcon={<SaveIcon />}
                        
                    >
                        Submit
                    </Button>
                    
                </form>
            </div>
        )
    }
}
