
import React from 'react'
import {signUpApi} from '../api/ApiHelper';
import {checkCardNumberAvailability, checkEmailAvailability} from '../api/ApiHelper';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save'

class SingUp extends React.Component {

    constructor(props) {
        super(props)
    
        this.state = {
            firstName:{
                value: ''
            },
            lastName:{
                value:''
            },
            address:{
                value:''
            },
            email:{
                value:''
            },
            cardNumber:{
                value: undefined
            },
            password:{
                value:undefined
            },
            confirmPass:{
                value: undefined
            }
        }
        
        this.isFormInvalid= this.isFormInvalid.bind(this);
        this.validateEmailAvailability=this.validateEmailAvailability.bind(this);
        this.validateCardNumbAvailability=this.validateCardNumbAvailability.bind(this);
        this.validateConfPasswordAbility = this.validateConfPasswordAbility.bind(this);
    }
    
    handleInputChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;        
        const inputValue = target.value;
        
        this.setState({
            [inputName] : {
                value: inputValue,
                ...validationFun(inputValue)
            }
        });
    }

    exporting=(e)=>{
        e.preventDefault();

        if(this.isFormInvalid){
            
            let B = {
                firstName: this.state.firstName.value,
                lastName: this.state.lastName.value,
                address: this.state.address.value,
                email: this.state.email.value,
                cardNumber:"8600"+this.state.cardNumber.value,
                password: this.state.password.value
            }

            // axios.post("http://localhost:4040/users/signIn", {
            // firstName:firstName, 
            // lastName:lastName, 
            // address:address,
            // email:email,
            // cardNumber:cardNumber,
            // password:password},{
            //     headers:{
            //         "Content-Type":"application/json"
            //     }
            // });

            signUpApi(B).then(res=>{
                console.log(res);
            })

        }
    }

    isFormInvalid() {

        return (
            this.state.firstName.validateStatus  &&
            this.state.lastName.validateStatus  &&
            this.state.address.validateStatus &&
            this.state.cardNumber.validateStatus &&
            this.state.email.validateStatus &&
            this.state.password.validateStatus
        );
    }


render(){
    return (
        <div>
            <form onSubmit={this.exporting}>
                {/* <label>{this.state.firstName.errorMsg}</label> */}
                {/* <input type="text"
                name="firstName"
                placeholder="Enter your FirstName"
                value={this.state.firstName.value}
                onChange={(event) => this.handleInputChange(event, this.validateFirstName)} />  */}


                <TextField 
                    // id="outlined-basic" 
                    type="text"
                    name="firstName"
                    error={this.state.firstName.validateStatus}
                    placeholder="First Name"
                    value={this.state.firstName.value}
                    onChange={(event) => this.handleInputChange(event, this.validateFirstName)}
                    label="First Name" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    helperText={this.state.firstName.errorMsg}
                    />


                {/* <label>{this.state.lastName.errorMsg}</label> */}
                {/* <input type="text" 
                name="lastName" 
                placeholder="Enter your lastName" 
                value={this.state.lastName.value} 
                onChange={(event) => this.handleInputChange(event, this.validateFirstName)} /> */}

                <TextField 
                    // id="outlined-basic" 
                    type="text"
                    name="lastName"
                    error={this.state.lastName.validateStatus}
                    placeholder="Last Name"
                    value={this.state.lastName.value}
                    onChange={(event) => this.handleInputChange(event, this.validateFirstName)}
                    label="Last Name" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    helperText={this.state.lastName.errorMsg}
                    />

                {/* <label>{this.state.address.errorMsg}</label> */}
                {/* <input type="text" 
                name="address" 
                placeholder="Enter your address"
                value={this.state.address.value}
                onChange={(event) => this.handleInputChange(event, this.validateAddress)} /> */}

<TextField 
                    // id="outlined-basic" 
                    type="text"
                    name="address"
                    placeholder="Your Address"
                    error={this.state.address.validateStatus}
                    value={this.state.address.value}
                    onChange={(event) => this.handleInputChange(event, this.validateAddress)}
                    label="Your Address" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    helperText={this.state.address.errorMsg}
                    />

{/* <label>{this.state.email.errorMsg}</label> */}
                {/* <input type="email"
                name="email" 
                placeholder="Enter your email" 
                value={this.state.email.value} 
                onChange={ (event) => this.handleInputChange(event, this.validateEmail)} 
                onBlur={this.validateEmailAvailability} /> */}

<TextField 
                    // id="outlined-basic" 
                    type="email"
                    name="email"
                    placeholder="Email"
                    error={this.state.email.validateStatus}
                    value={this.state.email.value}
                    onChange={(event) => this.handleInputChange(event, this.validateEmail)}
                    label="Email" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    helperText={this.state.email.errorMsg}
                    />

{/* <label>{this.state.cardNumber.errorMsg}</label> */}
                {/* <input type="number" 
                name="cardNumber" 
                placeholder="Enter your cardNumber" 
                value={this.state.cardNumber.value} 
                onChange={(event) => this.handleInputChange(event, this.validateCardNumber)} 
                onBlur={this.validateCardNumbAvailability} /> */}

<TextField 
                    // id="outlined-basic" 
                    type="number"
                    name="cardNumber"
                    placeholder="CardNumber"
                    error={this.state.cardNumber.validateStatus}
                    value={this.state.cardNumber.value}
                    onChange={(event) => this.handleInputChange(event, this.validateCardNumber)}
                    label="CardNumber" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            8600 
                          </InputAdornment>
                        ),
                      }}
                    helperText={this.state.cardNumber.errorMsg}
                    />

{/* <label>{this.state.password.errorMsg}</label> */}
                {/* <input type="password" 
                name="password" 
                placeholder="Enter your password" 
                value={this.state.password.value} 
                onChange={(event) => this.handleInputChange(event, this.validateFirstName)} /> */}

<TextField 
                    // id="outlined-basic" 
                    type="password"
                    name="password"
                    error={this.state.password.validateStatus}
                    placeholder="Password"
                    value={this.state.password.value}
                    onChange={(event) => this.handleInputChange(event, this.validateFirstName)}
                    label="Password" 
                    variant="outlined" 
                    size ="small"
                    margin ='dense'
                    helperText={this.state.password.errorMsg}
                    />

{/* <label>{this.state.confirmPass.errorMsg}</label> */}
                {/* <input type="password" 
                name="confirmPass" 
                placeholder="Confirm the password" 
                value={this.state.confirmPass.value} 
                onChange={(event) => this.handleInputChange(event, this.validateFirstName)} /> */}

<TextField 
                    // id="outlined-basic" 
                    autoFocus
                    error={this.state.confirmPass.validateStatus}
                    type="password"
                    margin ='dense'
                    name="confirmPass"
                    placeholder="Confirm the password"
                    value={this.state.confirmPass.value}
                    onChange={(event) => this.handleInputChange(event, this.validateConfPasswordAbility)}
                    label="Confirm the password" 
                    variant="outlined" 
                    size ="small"
                    helperText={this.state.confirmPass.errorMsg}
                    />

                {/* <button>Submit</button> */}
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    startIcon={<SaveIcon />}
                >
                    Submit
                </Button>
                

            </form>
        </div>
    )
}


validateFirstName=(firstName)=>{
    if(firstName !==''){
    if(firstName.length < 3) {
        return {
            validateStatus: true,
            errorMsg: `Name is too short (Minimum 3 characters needed.)`
        }
    } else if (firstName.length > 10) {
        return {
            validationStatus: true,
            errorMsg: `Name is too long (Maximum 10 characters allowed.)`
        }
    } else {
        return {
            validateStatus: false,
            errorMsg: null,
        };            
    }
}
}
    validateAddress=(address)=>{
        if(address!==''){
            if(address < 10){
                return {
                    validationStatus: true,
                    errorMsg: `Address must be higher then 10 character`
                }
            }else if(address > 30){
                return {
                    validationStatus: true,
                    errorMsg: `Address is too long (Maximum 30 characters allowed.)`
                }
            }else {
                return {
                    validationStatus: false,
                    errorMsg: null
                }
            }
        }
    }

    validateEmail = (email) => {
        if(!email) {
            return {
                validateStatus: true,
                errorMsg: 'Email may not be empty'                
            }
        }

        const EMAIL_REGEX = RegExp('[^@ ]+@[^@ ]+\\.[^@ ]+');
        if(!EMAIL_REGEX.test(email)) {
            return {
                validateStatus: true,
                errorMsg: 'Email not valid'
            }
        }

        if(email.length > 40) {
            return {
                validateStatus: true,
                errorMsg: `Email is too long (Maximum 40 characters allowed)`
            }
        }

        return {
            validateStatus: null,
            errorMsg: null
        }
    }

    validateCardNumber=(cardNumber)=>{
        if(cardNumber.length !== 12){
            return {
                validateStatus: true,
                errorMsg: 'card number must be 16 digits'
            }
        }
        return {
            validateStatus: null,
            errorMsg: null
        }
    }

    validateCardNumbAvailability() {
        // First check for client side errors in username
        const cardNumbValue = this.state.cardNumber.value;
        const usernameValidation = this.validateCardNumber(cardNumbValue);

        if(usernameValidation.validateStatus === true) {
            this.setState({
                cardNumber: {
                    value: cardNumbValue,
                    ...usernameValidation
                }
            });
            return;
        }

        this.setState({
            cardNumber: {
                value: cardNumbValue,
                validateStatus: false,
                errorMsg: null
            }
        });

        checkCardNumberAvailability(cardNumbValue)
        .then(response => {
            // if(response) {
            //     this.setState({
            //         cardNumber: {
            //             value: cardNumbValue,
            //             validateStatus: 'success',
            //             errorMsg: null
            //         }
            //     });
            // } else {
            //     this.setState({
            //         cardNumber: {
            //             value: cardNumbValue,
            //             validateStatus: 'error',
            //             errorMsg: 'This username is already taken'
            //         }
            //     });
            // }
            console.log(response)
        }).catch(error => {
            // Marking validateStatus as success, Form will be recchecked at server
            this.setState({
                cardNumber: {
                    value: cardNumbValue,
                    validateStatus: 'error',
                    errorMsg: null
                }
            });
        });
    }

    validateEmailAvailability() {
        // First check for client side errors in email
        const emailValue = this.state.email.value;
        const emailValidation = this.validateEmail(emailValue);

        if(emailValidation.validateStatus === true) {
            this.setState({
                email: {
                    value: emailValue,
                    ...emailValidation
                }
            });    
            return;
        }

        this.setState({
            email: {
                value: emailValue,
                validateStatus: false,
                errorMsg: null
            }
        });

        checkEmailAvailability(emailValue)
        .then(response => {
            // if(response.available) {
            //     this.setState({
            //         email: {
            //             value: emailValue,
            //             validateStatus: 'success',
            //             errorMsg: null
            //         }
            //     });
            // } else {
            //     this.setState({
            //         email: {
            //             value: emailValue,
            //             validateStatus: 'error',
            //             errorMsg: 'This Email is already registered'
            //         }
            //     });
            // }
            console.log(response);
            console.log(response.available);
        }).catch(error => {
            // Marking validateStatus as success, Form will be recchecked at server
            this.setState({
                email: {
                    value: emailValue,
                    validateStatus: 'error',
                    errorMsg: null
                }
            });
        });
    }

    validateConfPasswordAbility(confPassword){
        if (confPassword === this.state.password.value){
            return{
                validateStatus: false,
                errorMsg: null
            }
        }else{
            return {
                validateStatus: true,
                errorMsg: 'this is not confirm'
            }
        }
    }


















}
export default SingUp;