import React from 'react'
import { Link, withRouter} from 'react-router-dom'

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { ListItem, ListItemText, ListItemIcon, List } from '@material-ui/core';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      
      
    },
    menuButton: {
      // marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    },
    Button:{
      marginRight: theme.spacing(3)
    },
    listItem:{
      textDecoration: 'none',
      color: theme.palette.text.primary,
      fontSize: 2
    },
    link:{
      marginLeft: theme.spacing(5),
      fontSize: 8
    }
  }));
  

function Header(){

    const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" >
        <Container maxWidth="xl">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography 
          variant="h6" 
          className={classes.title}>
            News
          </Typography>
              <Link to="/signUp" className={classes.link}>
                <ListItem button className={classes.listItem}>
                  <ListItemText primary={"Sign Up"} fontSize='2px'/>
                </ListItem>
              </Link>
              <Link to="/" className={classes.link}>
                <ListItem button className={classes.listItem}>
                  <ListItemText primary={"Log In"} />
                </ListItem>
              </Link>
              
            
        </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}

// class Header extends Component {
    
    
//     render() {
//         if(!this.props.isAuthenticated){   
//         return (
//             <AppBar>
//                 <Toolbar>
//             <Tabs
//           indicatorColor="primary"
//           textColor="primary"
//           variant="fullWidth"
//           aria-label="full width tabs example"
//         >
//           <Tab label="Item One" />
//           <Tab label="Item Two" />
//           <Tab label="Item Three" />
//         </Tabs>
//         </Toolbar>
//         </AppBar>
//         )
//         }else{
//            return(
//             <div className="container">
//             <h3>
//                OMAD LI
//             </h3>
//             <p>{this.props.currentUser.lastName +" "}
//             {this.props.currentUser.firstName +" "}
//             {this.props.currentUser.email}</p>
//             <ul className="nav nav-tabs justify-content-end" role="tablist">

//                 <li className="nav-item mr-sm-5">
//                     <Link className="nav-link"  to="/">
//                        UY
//                     </Link >
//                 </li>
                
//                 <li className="nav-item mr-sm-5">
//                     <Link className="nav-link "  to="/settings">Settings
//                     </Link >
//                 </li>

//                 <li className="nav-item mr-sm-5">
//                    <button className="nav-link " onClick={this.props.logout}> logout</button>
//                 </li>
                
//             </ul>
//         </div>
//            )
//         }

//     }
// }

export default withRouter(Header);
