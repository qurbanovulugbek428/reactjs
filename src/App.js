import React,{Component} from 'react';
import { BrowserRouter, Route , Switch, withRouter } from 'react-router-dom';
import Header from './utills/Header';
import Login from './utills/Login';
import Home from './utills/Home';
import SingUp from './utills/SingUp';
import {getCurrentUser} from './api/ApiHelper';
import {ACCESS_TOKEN, USER_ID} from './constants/enams';
import Settings from './utills/Settings';
// import PropTypes from "prop-types";

class App extends Component {
  
  // static propTypes = {
  //   match: PropTypes.object.isRequired,
  //   location: PropTypes.object.isRequired,
  //   history: PropTypes.object.isRequired
  // };

  constructor(props){
    super(props);

    this.state={
      currentUser: [],
      isAuthenticated: false,
      isLoading: false
    }
    // this.loadCurrentUser = this.loadCurrentUser.bind(this);
  }

  loadCurrentUser=()=>{
    // this.setState({
    //   isLoading: true
    // });
    if(localStorage.getItem(ACCESS_TOKEN)){
    getCurrentUser().then(response => {
      console.log(response.data);
      this.setState({
        currentUser: response.data,
        isAuthenticated: true,
        isLoading: false
      });
      // this.props.history.push(`/accessHome/${response.username}`);
      this.props.history.push('/');
      // this.context.history.push('/');
    }).catch(error => {

      // this.setState({
      //   isLoading: false
      // });  
    });
  }
  }
  
  

  componentDidMount(){
      this.loadCurrentUser();
  }
  // componentWillMount(){
  //   this.loadCurrentUser();
  // }
  
  handleLogin=()=> {
    this.loadCurrentUser();
    
  }

  handleLogOut=()=> {
    localStorage.removeItem(ACCESS_TOKEN);
    localStorage.removeItem(USER_ID);
    this.setState({
      currentUser: [],
      isAuthenticated: false
    });
    this.props.history.push('/logIn');
    
    
  }

render(){
  // const { match, location, history } = this.props;

  console.log('this is app from')
  console.log(this.state.currentUser.firstName);
    if(!this.state.isAuthenticated){
      return (
        <>
        <BrowserRouter>
         <Header RouterisAuthenticated={this.state.isAuthenticated} currentUser={this.state.currentUser}></Header>
            <Switch>
              <Route path='/home' exact component={Home}></Route>
              <Route path="/signUp" exact component={SingUp} ></Route>
              {/* <Route path={["/login","/"]} component={Login} ></Route> */}
              {/* <Route path="/" component={Login} ></Route> */}
              <Route path={["/login","/"]} render={(props) => <Login onLogin={this.handleLogin} />} />
            </Switch>
        </BrowserRouter>
        </>
      );
    }else{
      return (
        <BrowserRouter>
        <Header isAuthenticated={this.state.isAuthenticated} currentUser={this.state.currentUser} logout={this.handleLogOut}></Header>
            <Switch>
              <Route path='/' exact component={Home}></Route>
              <Route path='/settings' exact component={Settings}></Route>
            </Switch>
        </BrowserRouter>
      );
    }
  
}
}


export default withRouter(App);
