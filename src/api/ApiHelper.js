import axios from 'axios';
import {ACCESS_TOKEN, API_BASE_URL} from '../constants/enams';

const request = (options) => {
    // const header =new Headers({});

    
        // header.append('Authorization', localStorage.getItem(ACCESS_TOKEN));
        // console.log(header);
            let header;
        if(localStorage.getItem(ACCESS_TOKEN)){
         header={
                'Authorization': localStorage.getItem(ACCESS_TOKEN)
            }
        }
        console.log(header);

    // const defaults = {headers: header};
    // const option = Object.assign(options, defaults);
    if(options.method==='GET'){
        return axios.get(options.url,{headers: header}
        )
            // .then(response => 
            //     response.json().then(json => {
            //         console.log(json);
            //         console.log(response);
            //         if(!response.ok) {
            //             return Promise.reject(json);
            //         }
            //         return json;
            //     })
            // );
    }else if(options.method === 'POST'){
        console.log('apihlp');
        console.log(options.body);
        return axios.post(options.url,options.body,{headers: header})
            // .then(response => {console.log(response)}
                // response.json().then(json => {
                //     console.log(json);
                //     if(!response.ok) {
                //         return Promise.reject(json);
                //     }
                //     return json;
                // })
            // );
    }else if(options.method==='PUT'){
        return axios.post(options.url, options.body,{headers:  header})
            // .then(response => 
            //     response.json().then(json => {
            //         if(!response.ok) {
            //             return Promise.reject(json);
            //         }
            //         return json;
            //     })
            // );
    }
    
};

export function loginApi(loginRequest) {
    return request({
        url: API_BASE_URL + "/auth/login",
        method: 'POST',
        body: loginRequest
    });
}

export function signUpApi(signupRequest) {
    return request({
        url: API_BASE_URL + "/auth/signIn",
        method: 'POST',
        body: signupRequest
    });
}

export function checkCardNumberAvailability(cardNumber) {
    return request({
        url: API_BASE_URL + "/auth/checkCardNumb?cardNumber=" + cardNumber,
        method: 'GET'
    });
}

export function checkEmailAvailability(email) {
    return request({
        url: API_BASE_URL + "/auth/checkEmailAvailability?email=" + email,
        method: 'GET'
    });
}


export function getCurrentUser() {
    if(localStorage.getItem(ACCESS_TOKEN)) {
        return request({
            url: API_BASE_URL + "/auth/getCurrentUser",
            method: 'GET'
        });
    }
    return;
}

export function getUserProfile(email) {
    return request({
        url: API_BASE_URL + "/users/" + email,
        method: 'GET'
    });
}